import "./Navigation.css";
import { createBrowserRouter, NavLink, RouterProvider } from "react-router-dom"

function Navigation() {
  return (
    <ul>
      {/* Pour chaque lien que l'on veut créer on indique le chemin que va emprunter ce lien (et les routes ont déjà été déclarées dans main) */}
      <nav className="Navigation">
        <NavLink to="/">Home</NavLink>
        <NavLink to="/products">Products</NavLink>
        <NavLink to="/cart">Cart</NavLink>
      </nav>
    </ul>
  );
}

export default Navigation;
