import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom"
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";


// Création des différentes routes et appels des composants à afficher pour ces routes
// path indique le chemin qui s'affiche dans l'URL (et pas le chemin des fichiers en local -_-')
const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/products",
    element: <ProductList />,
  },
  {
    path: "/cart",
    element: <Cart />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>

    {/* Ici on utilise les routes déjà déclarées plus haut */}
    <RouterProvider router={router} />
    
    {/* Plus besoin de ces composants puisqu'on est sur la page home */}
      {/* <ProductList /> */}
      {/* <Cart /> */}
  </React.StrictMode>
);
